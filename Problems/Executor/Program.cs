﻿using Problems;
using Problems.Shared;

namespace Executor;

public class Program
{
    static void Main(string[] args)
    {
        var listNode = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4))));

        var swapNode = new SwapNodesInPairs();

        var resultHead = swapNode.SwapPairs(listNode);

        while (resultHead != null)
        {
            Console.WriteLine(resultHead.val);
            resultHead = resultHead.next;
        }
    }
}
