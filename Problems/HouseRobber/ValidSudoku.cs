﻿using System.ComponentModel;
using System.Data.Common;
using System.Net.NetworkInformation;

namespace Problems;

/// <summary>
/// Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:
///
///Each row must contain the digits 1-9 without repetition.
///Each column must contain the digits 1-9 without repetition.
///Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.
///
///Note:
///
//A Sudoku board (partially filled) could be valid but is not necessarily solvable.
///Only the filled cells need to be validated according to the mentioned rules.
///
///Example 1:
///
///Input: board =
///[["5", "3", ".", ".", "7", ".", ".", ".", "."],
///["6", ".", ".", "1", "9", "5", ".", ".", "."],
///[".", "9", "8", ".", ".", ".", ".", "6", "."],
///["8", ".", ".", ".", "6", ".", ".", ".", "3"],
///["4", ".", ".", "8", ".", "3", ".", ".", "1"],
///["7", ".", ".", ".", "2", ".", ".", ".", "6"],
///[".", "6", ".", ".", ".", ".", "2", "8", "."],
///[".", ".", ".", "4", "1", "9", ".", ".", "5"],
///[".", ".", ".", ".", "8", ".", ".", "7", "9"]]
///Output: true
///
///Example 2:
///
///Input: board =
///[["8", "3", ".", ".", "7", ".", ".", ".", "."],
///["6", ".", ".", "1", "9", "5", ".", ".", "."],
///[".", "9", "8", ".", ".", ".", ".", "6", "."],
///["8", ".", ".", ".", "6", ".", ".", ".", "3"],
///["4", ".", ".", "8", ".", "3", ".", ".", "1"],
///["7", ".", ".", ".", "2", ".", ".", ".", "6"],
///[".", "6", ".", ".", ".", ".", "2", "8", "."],
///[".", ".", ".", "4", "1", "9", ".", ".", "5"],
///[".", ".", ".", ".", "8", ".", ".", "7", "9"]]
///Output: false
///
///Explanation: Same as Example 1, except with the 5 in the top left corner being modified to 8. Since there are two 8's in the top left 3x3 sub-box, it is invalid.
///
///Constraints:
///
///board.length == 9
///board[i].length == 9
///board[i][j] is a digit 1-9 or '.'.
/// </summary>
public class ValidSudoku
{
    private const int Step = 3;

    private HashSet<char>[] rowsValidation = new HashSet<char>[9];
    private HashSet<char>[] columnsValidation = new HashSet<char>[9];
    private HashSet<char>[] squaresValidation = new HashSet<char>[9];

    public bool IsValidSudoku(char[][] board)
    {
        for(int index = 0; index < board.Length;  index++)
        {
            rowsValidation[index] = new HashSet<char>();
            columnsValidation[index] = new HashSet<char>();
            squaresValidation[index] = new HashSet<char>();
        }

        for(int row = 0; row < 9; row++)
        {
            for(int column = 0;  column < 9; column++)
            {
                int squareIndex = (row/3)*Step + column/Step;

                var symbol = board[row][column];

                if (board[row][column] != '.')
                {
                    if (!rowsValidation[row].Add(symbol))
                    {
                        return false;
                    }

                    if (!columnsValidation[column].Add(symbol))
                    {
                        return false;
                    }

                    if (!squaresValidation[squareIndex].Add(symbol))
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
