﻿using System;

namespace Problems;

/// <summary>
/// There is an m x n grid with a ball. The ball is initially at the position [startRow, startColumn]. 
/// You are allowed to move the ball to one of the four adjacent cells in the grid (possibly out of the grid crossing the grid boundary). 
/// You can apply at most maxMove moves to the ball.
///
///Given the five integers m, n, maxMove, startRow, startColumn, return the number of paths to move the ball out of the grid boundary.
///Since the answer can be very large, return it modulo 109 + 7.
///
///Example 1:
///
///Input: m = 2, n = 2, maxMove = 2, startRow = 0, startColumn = 0
///Output: 6
///
///Example 2:
///
///Input: m = 1, n = 3, maxMove = 3, startRow = 0, startColumn = 1
///Output: 12
///
///Constraints:
///1 <= m, n <= 50
///0 <= maxMove <= 50
///0 <= startRow<m
///0 <= startColumn<n
/// </summary>
public class OutOfBoundaryPaths
{
    const int Mod = 1_000_000_000 + 7;
    private int rows;
    private int columns;
    private int maxMove;

    int[,,] dp;

    public int FindPaths(int m, int n, int maxMove, int startRow, int startColumn)
    {
        rows = m;
        columns = n;
        this.maxMove = maxMove;
        dp = new int[rows, columns, maxMove + 1];

        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                for (int move = 0; move <= maxMove; move++)
                {
                    dp[row, column, move] = -1;
                }
            }
        }

        return PathsCount(startRow, startColumn, 0);
    }

    private int PathsCount(int row, int column, int move)
    {
        if (move > maxMove)
        {
            return 0;
        }

        if (row < 0 || row >= rows || column < 0 || column >= columns)
        {
            return 1;
        }

        if (dp[row, column, move] != -1)
        {
            return dp[row, column, move];
        }

        int count = 0;
        count = (count + PathsCount(row + 1, column, move + 1)) % Mod;
        count = (count + PathsCount(row, column + 1, move + 1)) % Mod;
        count = (count + PathsCount(row - 1, column, move + 1)) % Mod;
        count = (count + PathsCount(row, column - 1, move + 1)) % Mod;

        dp[row, column, move] = count;

        return count;
    }
}
