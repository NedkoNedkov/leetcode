﻿namespace Problems;

public class EvaluateReversePolishNotation
{
    public int EvalRPN(string[] tokens)
    {
        Stack<int> numbers = new Stack<int>();

        HashSet<string> operators = new HashSet<string>() { "+","-","*","/"};

        foreach (string token in tokens)
        {
            if (operators.Contains(token))
            {
                var operand2 = numbers.Pop();
                var operant1 = numbers.Pop();

                var operationResult = token switch
                {
                    "+" => operant1 + operand2,
                    "-" => operant1 - operand2,
                    "*" => operant1 * operand2,
                    "/" => operant1 / operand2,
                };

                numbers.Push(operationResult);
            }
            else
            {
                numbers.Push(int.Parse(token));
            }
        }

        return numbers.Pop();
    }
}
