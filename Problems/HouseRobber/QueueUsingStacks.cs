﻿namespace Problems;

public class QueueUsingStacks
{
    private Stack<int> inputStack;
    private Stack<int> outputStack;

    public QueueUsingStacks()
    {
        inputStack = new Stack<int>();
        outputStack = new Stack<int>();
    }

    public void Push(int x)
    {
        inputStack.Push(x);
    }

    public int Pop()
    {
        MoveInputToOutput();

        return outputStack.Pop();
    }

    public int Peek()
    {
        MoveInputToOutput();

        return outputStack.Peek();
    }

    public bool Empty()
    {
        return (inputStack.Count + outputStack.Count) == 0;
    }

    private void MoveInputToOutput()
    {
        if(outputStack.Count == 0)
        {
            while(inputStack.Count > 0)
            {
                outputStack.Push(inputStack.Pop());
            }
        }
    }
}
