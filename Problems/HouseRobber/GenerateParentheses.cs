﻿namespace Problems;

/// <summary>
/// Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
///
///Example 1:
///
///Input: n = 3
///Output: ["((()))","(()())","(())()","()(())","()()()"]
///
///Example 2:
///
///Input: n = 1
///Output: ["()"]
///
///Constraints:
///
///1 <= n <= 8
/// </summary>
public class GenerateParentheses
{
    private IList<string> combinations = new List<string>();
    private int maxLenght;
    private int count;

    public IList<string> GenerateParenthesis(int n)
    {
        maxLenght = n * 2;
        count = n;

        GenerateParenthesis("(", 1, 0);

        return combinations;
    }

    private void GenerateParenthesis(string combination, int openCount, int closedCount)
    {
        if (combination.Length == maxLenght)
        {
            combinations.Add(combination);
        }

        if (openCount < count)
        {
            GenerateParenthesis(combination + "(", openCount + 1, closedCount);
        }

        if (closedCount < openCount)
        {
            GenerateParenthesis(combination + ")", openCount, closedCount + 1);
        }
    }

    public string GenerateParenthesesAsString(int n)
    {
        return $"{string.Join("\n",GenerateParenthesis(n))}";
    }
}
