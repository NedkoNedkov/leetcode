﻿using Problems.Shared;
using System.Collections.Generic;
using System.Globalization;

namespace Problems;

/// <summary>
/// You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.
///
///Merge all the linked-lists into one sorted linked-list and return it.
///
///Example 1:
///
///Input: lists = [[1,4,5],[1,3,4],[2,6]]
///Output: [1,1,2,3,4,4,5,6]
///Explanation: The linked-lists are:
///[
///  1->4->5,
///  1->3->4,
///  2->6
///]
///merging them into one sorted list:
///1->1->2->3->4->4->5->6
///
///Example 2:
///
///Input: lists = []
///Output: []
///
///Example 3:
///
///Input: lists = [[]]
///Output: []
///
///Constraints:
///
///k == lists.length
///0 <= k <= 10^4
///0 <= lists[i].length <= 500
///-10^4 <= lists[i][j] <= 10^4
///lists[i] is sorted in ascending order.
///The sum of lists[i].length will not exceed 10^4
/// </summary>
public class MergeKSortedLists
{
    public ListNode MergeKLists(ListNode[] lists)
    {
        if (lists.Count() == 0) return new ListNode();
        if(lists.Count() == 1) return lists[0];

        ListNode list = lists[0];

        for(int i = 1; i < lists.Count(); i++)
        {
            list = MergeList(list, lists[i]);
        }

        return list;
    }

    private ListNode MergeList(ListNode list1, ListNode list2)
    {
        if(list1 == null)
        {
            return list2;
        }

        if(list2 == null)
        {
            return list1;
        }

        if(list1.val <= list2.val)
        {
            list1.next = MergeList(list1.next, list2);
            return list1;
        }
        else
        {
            list2.next = MergeList(list1, list2.next);
            return list2;
        }
    }
}
