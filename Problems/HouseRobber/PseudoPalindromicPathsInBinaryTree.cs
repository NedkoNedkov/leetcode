﻿
using Problems.Shared;

namespace Problems;

/// <summary>
/// Given a binary tree where node values are digits from 1 to 9. A path in the binary tree is said to be pseudo-palindromic if at least one permutation 
/// of the node values in the path is a palindrome.
///
///Return the number of pseudo-palindromic paths going from the root node to leaf nodes.
///
///Example 1:
///Input: root = [2, 3, 1, 3, 1, null, 1]
///Output: 2 
///Explanation: The figure above represents the given binary tree. There are three paths going from the root node to leaf nodes: the red path[2, 3, 3], 
///the green path[2, 1, 1], and the path[2, 3, 1]. Among these paths only red path and green path are pseudo-palindromic paths since the red path [2,3,3] 
///can be rearranged in [3, 2, 3] (palindrome) and the green path[2, 1, 1] can be rearranged in [1, 2, 1] (palindrome).
///
///Example 2:
///Input: root = [2, 1, 1, 1, 3, null, null, null, null, null, 1]
///Output: 1 
///Explanation: The figure above represents the given binary tree. There are three paths going from the root node to leaf nodes: the green path[2, 1, 1], 
///the path [2,1,3,1], and the path[2, 1]. Among these paths only the green path is pseudo-palindromic since [2,1,1] can be rearranged in [1, 2, 1] (palindrome).
///
///Example 3:
///Input: root = [9]
///Output: 1
///
///Constraints:
///The number of nodes in the tree is in the range [1, 10^5].
///1 <= Node.val <= 9
/// </summary>
public class PseudoPalindromicPathsInBinaryTree
{
    public int PseudoPalindromicPaths(TreeNode root)
    {
        int result = 0;
        HashSet<int> digitMapper = new HashSet<int>();
        PseudoPaths(root, digitMapper,ref result);
        return result;
    }
    private void PseudoPaths(TreeNode root, HashSet<int> digitMapper,ref int result)
    {
        if (root == null)
        {
            return;
        }
        if(!digitMapper.Add(root.val))
        {
            digitMapper.Remove(root.val);
        }
        if(root.left == null && root.right == null)
        {
            if(digitMapper.Count() < 2)
            {
                result++;
            }
        }
        else
        {
            PseudoPaths(root.left, digitMapper,ref result);
            PseudoPaths(root.right, digitMapper,ref result);
        }

        if(!digitMapper.Remove(root.val))
        {
            digitMapper.Add(root.val);
        }
    }
}
