﻿using System.Xml.Linq;

namespace Problems;

/// <summary>
/// You are given an array of strings . A string is formed by the concatenation of a subsequence of that has unique characters.arrsarr
///
///Return the maximum possible length of.s
///
///A subsequence is an array that can be derived from another array by deleting some or no elements without changing the order of the remaining elements.
///
///Example 1:
///
///Input: arr = ["un", "iq", "ue"]
///Output: 4
///Explanation: All the valid concatenations are:
///- ""
///- "un"
///- "iq"
///- "ue"
///- "uniq" ("un" + "iq")
///- "ique" ("iq" + "ue")
///Maximum length is 4.
///
///Example 2:
///
///Input: arr = ["cha","r","act","ers"]
///Output: 6
///Explanation: Possible longest valid concatenations are "chaers" ("cha" + "ers") and "acters" ("act" + "ers").
///
///Example 3:
///
///Input: arr = ["abcdefghijklmnopqrstuvwxyz"]
///Output: 26
///Explanation: The only string in arr has all 26 characters.
///
///Constraints:
///
/// 1 <= arr.length <= 16
/// 1 <= arr[i].length <= 26
/// arr[i] contains only lowercase English letters.
/// </summary>
public class MaximumLengthConcatenatedString
{
    public int MaxLength(IList<string> arr)
    {
        return MaxLengthConcatenated(arr, 0, "");
    }

    private int MaxLengthConcatenated(IList<string> arr, int index, string s)
    {
        if(s.Distinct().Count() < s.Length)
        {
            return 0;
        }

        if(arr.Count() == index)
        {
            return s.Length;
        }

        return Math.Max(
            MaxLengthConcatenated(arr, index + 1, s),
            MaxLengthConcatenated(arr, index + 1, s + arr[index]));
    }
}
