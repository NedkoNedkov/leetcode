﻿using Problems.Shared;

namespace Problems;

public class SwapNodesInPairs
{
    public ListNode SwapPairs(ListNode head)
    {
        if (head == null)
        {
            return null;
        }
        if(head.next == null)
        {
            return head;
        }

        ListNode temp = new ListNode(0, head);
        ListNode prevNode = temp;
        ListNode currentNode = temp.next;


        while (currentNode != null && currentNode.next != null)
        {
            var firstNode = currentNode;
            var secondNode = currentNode.next;

            firstNode.next = secondNode.next;
            secondNode.next = firstNode;
            prevNode.next = secondNode;

            prevNode = firstNode;
            currentNode = prevNode.next;
        }

        return temp.next;
    }
}
